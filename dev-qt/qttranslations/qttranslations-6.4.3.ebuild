# Copyright 2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit qt6-build

DESCRIPTION="Translation files for the Qt6 framework"
KEYWORDS="~amd64"

DEPEND="=dev-qt/qtbase-${PV}*"
BDEPEND="=dev-qt/qttools-${PV}*[linguist]"
