# Copyright 2019-2023 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

LUA_COMPAT=( lua5-{1,4} )

inherit lua-single cmake

SRC_URI="https://github.com/pkulchenko/wxlua/archive/refs/tags/v3.2.0.2.tar.gz -> ${P}.tar.gz"
DESCRIPTION="Lua bindings for wxWidgets cross-platform GUI toolkit"
HOMEPAGE="https://github.com/pkulchenko/wxlua/"
LICENSE="wxWinLL-3.1"

SLOT="0"
KEYWORDS="~amd64"
IUSE="doc test"

REQUIRED_USE="${LUA_REQUIRED_USE}"

RESTRICT="!test? ( test )"

S="${WORKDIR}/${P}/wxLua"

PATCHES=(
   "${FILESDIR}/fix-install-path.patch"
)

BDEPEND="
   doc? ( app-doc/doxygen )
   test? ( dev-util/cppcheck )"
DEPEND=""
RDEPEND="
   ${DEPEND}
   ${LUA_DEPS}
   x11-libs/wxGTK"

src_configure() {

   local mycmakeargs=(
      -DBUILD_SHARED_LIBS=TRUE
      -DCMAKE_INSTALL_LIBDIR="/usr/$(get_libdir)"
      -DwxLua_LUA_LIBRARY_USE_BUILTIN=FALSE
      -DwxLua_LUA_LIBRARY_BUILD_SHARED=TRUE
      -DwxLua_LUA_LIBRARY_VERSION="$(ver_cut 1-2 $(lua_get_version))"
      -DwxLua_LUA_LIBRARY="$(lua_get_shared_lib)"
      -DwxLua_LUA_INCLUDE_DIR="$(lua_get_include_dir)"
      -DwxWidgets_COMPONENTS="stc;gl;html;aui;adv;core;net;base"
      -DwxLuaBind_COMPONENTS="stc;gl;html;aui;adv;core;net;base"
      -DwxWidgets_CONFIG_EXECUTABLE="/usr/bin/wx-config"
   )

   cmake_src_configure
}
