# Copyright 2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_HANDBOOK="optional"
ECM_TEST="optional"
VIRTUALX_REQUIRED="test"

KFMIN=5.92.0
QTMIN=5.15.3

inherit ecm

MY_PN="${PN}-${PN}@${PV}"
SRC_URI="https://gitlab.com/kwinft/${PN}/-/archive/${PN}@${PV}/${MY_PN}.tar.gz -> ${P}.tar.gz"

DESCRIPTION="Wayland compositor and X11 window manager forked from KWin"
HOMEPAGE="https://gitlab.com/kwinft/kwinft"
LICENSE="GPL-2+"

SLOT="5"
KEYWORDS="~amd64"
IUSE="caps gles2-only multimedia tools"

RESTRICT="test"

PVCUT=$(ver_cut 1-3)
S="${WORKDIR}/${PN}-${P/-/@}"

COMMON_DEPEND="
   >=dev-libs/libinput-1.14
   >=dev-libs/wayland-1.2
   >=dev-qt/qtdbus-${QTMIN}:${SLOT}
   >=dev-qt/qtdeclarative-${QTMIN}:${SLOT}
   >=dev-qt/qtgui-${QTMIN}:${SLOT}=[gles2-only=]
   >=dev-qt/qtscript-${QTMIN}:${SLOT}
   >=dev-qt/qtsensors-${QTMIN}:${SLOT}
   >=dev-qt/qtwidgets-${QTMIN}:${SLOT}
   >=dev-qt/qtx11extras-${QTMIN}:${SLOT}
   >=gui-libs/wlroots-0.14:= <gui-libs/wlroots-0.15:=
   =gui-libs/wrapland-$(ver_cut 1-2)*:${SLOT}
   >=kde-frameworks/kactivities-${KFMIN}:${SLOT}
   >=kde-frameworks/kauth-${KFMIN}:${SLOT}
   >=kde-frameworks/kcmutils-${KFMIN}:${SLOT}
   >=kde-frameworks/kcompletion-${KFMIN}:${SLOT}
   >=kde-frameworks/kconfig-${KFMIN}:${SLOT}
   >=kde-frameworks/kconfigwidgets-${KFMIN}:${SLOT}
   >=kde-frameworks/kcoreaddons-${KFMIN}:${SLOT}
   >=kde-frameworks/kcrash-${KFMIN}:${SLOT}
   >=kde-frameworks/kdeclarative-${KFMIN}:${SLOT}
   >=kde-frameworks/kglobalaccel-${KFMIN}:${SLOT}=
   >=kde-frameworks/ki18n-${KFMIN}:${SLOT}
   >=kde-frameworks/kiconthemes-${KFMIN}:${SLOT}
   >=kde-frameworks/kidletime-${KFMIN}:${SLOT}=
   >=kde-frameworks/kio-${KFMIN}:${SLOT}
   >=kde-frameworks/knewstuff-${KFMIN}:${SLOT}
   >=kde-frameworks/knotifications-${KFMIN}:${SLOT}
   >=kde-frameworks/kpackage-${KFMIN}:${SLOT}
   >=kde-frameworks/kservice-${KFMIN}:${SLOT}
   >=kde-frameworks/ktextwidgets-${KFMIN}:${SLOT}
   >=kde-frameworks/kwayland-${KFMIN}:${SLOT}
   >=kde-frameworks/kwidgetsaddons-${KFMIN}:${SLOT}
   >=kde-frameworks/kwindowsystem-${KFMIN}:${SLOT}[X]
   >=kde-frameworks/kxmlgui-${KFMIN}:${SLOT}
   >=kde-frameworks/plasma-${KFMIN}:${SLOT}
   >=kde-plasma/breeze-${PVCUT}:${SLOT}
   >=kde-plasma/kdecoration-${PVCUT}:${SLOT}
   >=kde-plasma/kscreenlocker-${PVCUT}:${SLOT}
   media-libs/fontconfig
   media-libs/freetype
   media-libs/libepoxy
   media-libs/mesa[wayland,X(+)]
   virtual/libudev:=
   x11-libs/libICE
   x11-libs/libSM
   x11-libs/libX11
   x11-libs/libXi
   x11-libs/libdrm
   >=x11-libs/libxcb-1.10
   >=x11-libs/libxkbcommon-0.7.0
   x11-libs/xcb-util-cursor
   x11-libs/xcb-util-image
   x11-libs/xcb-util-keysyms
   x11-libs/xcb-util-wm
   caps? ( sys-libs/libcap )
   gles2-only? ( media-libs/mesa[gles2] )
"
RDEPEND="${COMMON_DEPEND}
   !kde-plasma/kwin:${SLOT}
   >=dev-qt/qtquickcontrols-${QTMIN}:${SLOT}
   >=dev-qt/qtquickcontrols2-${QTMIN}:${SLOT}
   >=dev-qt/qtvirtualkeyboard-${QTMIN}:${SLOT}
   >=kde-frameworks/kirigami-${KFMIN}:${SLOT}
   multimedia? ( >=dev-qt/qtmultimedia-${QTMIN}:${SLOT}[gstreamer,qml] )
"
DEPEND="${COMMON_DEPEND}
   >=dev-qt/designer-${QTMIN}:${SLOT}
   >=dev-qt/qtconcurrent-${QTMIN}:${SLOT}
   x11-base/xorg-proto
"
PDEPEND="
   >=kde-plasma/kde-cli-tools-${PVCUT}:${SLOT}
"

src_prepare() {
   ecm_src_prepare
   use multimedia || eapply "${FILESDIR}/kwin-5.16.80-gstreamer-optional.patch"
}

src_configure() {
   local mycmakeargs=(
       $(cmake_use_find_package caps Libcap)
       -DKWIN_BUILD_PERF=$(usex tools)
   )

   ecm_src_configure
}
