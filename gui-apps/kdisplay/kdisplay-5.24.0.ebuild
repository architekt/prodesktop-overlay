# Copyright 2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_TEST="forceoptional"

KFMIN=5.92.0
QTMIN=5.15.3

inherit ecm

MY_PN="${PN}-${PN}@${PV}"
SRC_URI="https://gitlab.com/kwinft/${PN}/-/archive/${PN}@${PV}/${MY_PN}.tar.gz -> ${P}.tar.gz"

DESCRIPTION="App and daemon for display managing"
HOMEPAGE="https://gitlab.com/kwinft/kdisplay/"
LICENSE="LGPL-2.1 GPL-2"

SLOT="0"
KEYWORDS="~amd64"
IUSE=""

RESTRICT="test"

S="${WORKDIR}/${MY_PN}"

DEPEND="
   >=kde-frameworks/extra-cmake-modules-${KFMIN}:5
   >=kde-frameworks/kdbusaddons-${KFMIN}:5
   >=kde-frameworks/kdeclarative-${KFMIN}:5
   >=kde-frameworks/ki18n-${KFMIN}:5
   >=kde-frameworks/kirigami-${KFMIN}:5
   =gui-libs/disman-$(ver_cut 1-2)*:5
"
RDEPEND="${DEPEND}"
   #>=dev-qt/qtsensors-${QTMIN}:5
   #>=dev-qt/qtdbus-${QTMIN}:5
   #>=dev-qt/qtdeclarative-${QTMIN}:5[widgets]
   #>=dev-qt/qtgui-${QTMIN}:5
   #>=dev-qt/qtwidgets-${QTMIN}:5
   #
   #>=kde-frameworks/kxmlgui-${KFMIN}:5
