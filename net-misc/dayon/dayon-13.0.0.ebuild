# Copyright 2024 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

inherit java-pkg-2 java-ant-2 desktop xdg

DESCRIPTION="Remote desktop assistance"
HOMEPAGE="https://retgal.github.io/Dayon"
SRC_URI="https://github.com/RetGal/Dayon/archive/refs/tags/v${PV}.tar.gz -> ${P}.tar.gz"
LICENSE="GPL-3"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="+assistant +assisted"

DEPEND=">=virtual/jdk-1.8:*"

RDEPEND=">=virtual/jre-1.8:*
         dev-java/ant-core:0"

S="${WORKDIR}/Dayon-${PV}"

EANT_BUILD_TARGET="dist"
EANT_GENTOO_CLASSPATH="ant-core"
JAVA_ANT_REWRITE_CLASSPATH="true"

src_install() {
   java-pkg_dojar build/${PN}.jar
   java-pkg_dolauncher ${PN} --jar ${PN}.jar --java_args "-Xmx256M"

   doicon dist/dayon.png

   if use assistant; then
      local PN="assistant"
      make_desktop_entry 'dayon assistant' "Dayon Assistant" "dayon.png"
   fi
   if use assisted; then
      local PN="assisted"
      make_desktop_entry 'dayon' "Dayon Assisted" "dayon.png"
   fi
}


