# Copyright 2022 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_QTHELP="true"
ECM_TEST="forceoptional"
VIRTUALX_REQUIRED="test"

KFMIN=5.92.0
QTMIN=5.15.3

inherit ecm

MY_PV=0.${PV/./}
SRC_URI="https://gitlab.com/kwinft/${PN}/-/archive/${PN}@${MY_PV}/${PN}-${PN}@${MY_PV}.tar.gz"

DESCRIPTION="Qt/C++ display management library"
HOMEPAGE="https://gitlab.com/kwinft/disman/"
LICENSE="GPL-2"

SLOT="5/7"
KEYWORDS="~amd64"
IUSE=""

RESTRICT="test"

S="${WORKDIR}/${PN}-${PN}@${MY_PV}"

DEPEND="
   >=kde-frameworks/extra-cmake-modules-${KFMIN}:5
   >=kde-frameworks/kwayland-${KFMIN}:5
   >=dev-qt/qtdbus-${QTMIN}:5
   >=dev-qt/qtgui-${QTMIN}:5
   >=dev-qt/qtx11extras-${QTMIN}:5
   =gui-libs/wrapland-$(ver_cut 1-2)*:5
   x11-libs/libxcb
"
RDEPEND="${DEPEND}"
