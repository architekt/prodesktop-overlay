# Copyright 2021 Architekt Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=8

ECM_TEST="forceoptional"

KFMIN=5.88.0
QTMIN=5.15.2

inherit ecm

MY_PV=0.${PV/./}
SRC_URI="https://gitlab.com/kwinft/${PN}/-/archive/${PN}@${MY_PV}/${PN}-${PN}@${MY_PV}.tar.gz"

DESCRIPTION="Qt/C++ library wrapping libwayland"
HOMEPAGE="https://gitlab.com/kwinft/wrapland"
LICENSE="LGPL-2.1+"

SLOT="5"
KEYWORDS="~amd64"
IUSE=""

RESTRICT="test"

S="${WORKDIR}/${PN}-${PN}@${MY_PV}"

RDEPEND="
   >=dev-libs/wayland-1.15.0
   >=dev-qt/qtconcurrent-${QTMIN}:${SLOT}
   >=dev-qt/qtgui-${QTMIN}:${SLOT}[egl]
   media-libs/mesa[egl]
"
DEPEND="${RDEPEND}
   >=dev-libs/wayland-protocols-1.22
"
